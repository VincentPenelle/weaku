This section is devoted to proving Lemma~\ref{lem:msowdef:dimtoinf}.
Our proof has two steps.
In Section~\ref{ssec:level1},
we show that in order to prove Lemma~\ref{lem:msowdef:dimtoinf},
it suffices to show the following lemma.
\begin{lemma}
	\label{lem:msowdef:ttoinf_and_dimttoinf}
	There is a formula \Aform[\Asetreset, \Asetinc] in \msoW
	which is true \iof
	the vector sequence \vecseqofsets\Asetreset\Asetinc is defined,
	%and
	satisfies
%	\[
%		\begin{array}{lcl}
			\toinf{\vecseqofsets\Asetreset\Asetinc} %&
			\text{and} %&
			\dimens{\vecseqofsets\Asetreset\Asetinc}\text{is unbounded.}
%		\end{array}
%	\]
\end{lemma}
Lemma~\ref{lem:msowdef:ttoinf_and_dimttoinf} itself will be proved in Section~\ref{ssec:level2}.

\subsection{From Lemma~\ref{lem:msowdef:ttoinf_and_dimttoinf} to Lemma~\ref{lem:msowdef:dimtoinf}}
\label{ssec:level1}
Recall that the vector sequence \vecseqofsets\Asetreset\Asetinc is defined
if and only if $R$ is infinite and disjoint from a set $I$,
which is clearly expressible in \mso.
So from now on, we will always consider vector sequences \vecseqofsets\Asetreset\Asetinc which are defined. 
\bigbreak

Given a vector sequence, a \emph{sub-sequence} is an infinite vector sequence obtained by dropping some of the vectors, like this:

\noindent\mypic{7}
An \emph{extraction} is obtained by removing some of the coordinates in some of the vectors,
but keeping at least one coordinate from each vector, like this:

\noindent\mypic{8}
In particular a vector sequence containing at least one empty vector has no extraction.
An extraction is called \emph{interval-closed}
if the coordinates which are kept in a given vector are consecutive, like this:

\noindent\mypic{9}
A \emph{$1$-extraction} is an extraction with only vectors of dimension $1$.
In particular, it is interval-closed.
A \emph{sub-extraction} is an extraction of a sub-sequence.
%, like this:
%
%\noindent\mypic{10}
%\bg{the last picture could be removed.}
\medbreak

The following lemma gives two conditions equivalent to the unboundedness of the dimension of a vector sequence.
\begin{lemma}
	\label{lem:unbounded degree}
	\label{lem:unbounded-dim}
	Let \Avecseq be a vector sequence.
	Then \dimens\Avecseq is unbounded \iof
	one of the following conditions is true:
	\begin{enumerate}[ref={\textcolor{darkgray}{\textbf{(\arabic*)}}},label={\textcolor{darkgray}{\textbf{(\arabic*)}}},leftmargin=2em,nosep]
		\item
			\label{it:tinfty+unbounded-dim}
			There exists a sub-extraction \Avecseqbis of \Avecseq
			such that \toinf\Avecseqbis and \dimens\Avecseqbis is unbounded.
		\item
			\label{it:bounded+unbunded-dim}
			There exists an interval-closed sub-extraction \Avecseqbis of \Avecseq
			such that
			\dimens\Avecseqbis is unbounded
			and
			\Avecseqbis is bounded,
			\ie, there exists a bound $\Abound\in\Nat$
			such that all the coordinates occurring in the sequence are less than \Abound.
	\end{enumerate}
\end{lemma}
\begin{proof}
	The if implication is clear,
	since admitting a sub-extraction of unbounded dimension
	trivially implies the unboundedness of the dimension.
	
	We now focus on the only-if implication.
	Assume that \dimens\Avecseq is unbounded
	and condition~\ref{it:tinfty+unbounded-dim} is false.
	Then,
	for all sub-extractions \Avecseqbis of \Avecseq
	we have that
	\toinf\Avecseqbis implies \dimens\Avecseqbis is bounded.
	This implies that
	there exists a constant \Athreshold
	such that for all vectors \Avect in \Avecseq,
	the number of coordinates greater than \Athreshold
	is less than \Athreshold.
	Indeed, if no such \Athreshold exists,
	we can exhibit a sub-extraction \Avecseqbis of \Avecseq,
	which tends towards infinity and has unbounded dimension.
	Let us mark the coordinates smaller than \Athreshold in every vector of \Avecseq.
	%
	For every integer \ANinteger, we can find a vector of \Avecseq with at least \ANinteger consecutive marked coordinates.
	Indeed suppose there is at most \ANinteger consecutive marked coordinates in every vector.
	Then, as there are at most \Athreshold unmarked coordinates, a vector cannot have dimension
	greater than $\ANinteger(\Athreshold+1) + \Athreshold$ (\ie, $\Athreshold + 1$ blocks of \ANinteger marked positions
	plus \Athreshold unmarked positions separating them).
	% 
	This contradicts that \dimens \Avecseq is unbounded.
	
	Thus by keeping for each vector one of the longest block of consecutive marked positions
	and removing vectors without any marked position,
	we can construct an interval-closed sub-extraction \Avecseqbis of \Avecseq which is 
	bounded by \Athreshold and has unbounded dimension.
\end{proof}

We now proceed to show how Lemma~\ref{lem:msowdef:ttoinf_and_dimttoinf} implies Lemma~\ref{lem:msowdef:dimtoinf}.
Recall that Lemma~\ref{lem:msowdef:dimtoinf} says that there is a formula \Aform[R,I] in \msoW
which is true if and only if the vector sequence \vecseqofsets\Asetreset\Asetinc is defined and satisfies
\dimens{\vecseqofsets\Asetreset\Asetinc} is unbounded.
\begin{proof}[Proof of Lemma~\ref{lem:msowdef:dimtoinf} assuming Lemma~\ref{lem:msowdef:ttoinf_and_dimttoinf}]
%	Assume that Lemma~\ref{lem:msowdef:ttoinf_and_dimttoinf} is true. 
	Consider \Asetreset and \Asetinc two sets of positions such that \vecseqofsets\Asetreset\Asetinc is defined.
	We will use Lemma~\ref{lem:unbounded-dim} to express in \msoW that \dimens{\vecseqofsets\Asetreset\Asetinc} is unbounded.
	The first condition of the lemma is expressible in \msoW by Lemma~\ref{lem:msowdef:ttoinf_and_dimttoinf},
	as shown in the next section.
	However, in order to express the second condition,
	we need that the gaps between consecutive intervals of $I$ have bounded length.
	This is ensured by extending the intervals as explained in the following.
	For any two disjoint sets $R$ and $I$, let us construct $J$ as follows:
	for every two consecutive positions $x<y$ in $I$ such that there is no element of $R$ in between,
	we add to $I$ all the positions between $x$ and $y$,
	except $y-1$, as in Figure~\ref{fig:completion}.
	The set $J$ is expressible in \mso, in the sense that there is a \mso-formula with three free set variables $R$, $I$, $J$
	which holds if and only if $J$ is obtained from $I$ by this process.
	Moreover, it is clear that \dimens{\vecseqofsets\Asetreset\Asetinc} is unbounded if and only if 
	\dimens{\vecseqofsets{\Asetreset}{J}} is unbounded
	since the dimension in both sequences are pairwise equal
	(see Figure~\ref{fig:completion}).
	It suffices thus to prove Lemma~\ref{lem:msowdef:dimtoinf} for sets $R$ and $I$ which satisfy:
	\begin{description}
		\item[{\namedlabel[$\mathbf{(\star)}$]{it:star}{$(\star)$}}]
			between two consecutive elements of $I$,
			there is either an element of $R$ or at most one position not in $I$.
	\end{description}
	\begin{figure}[h]
		\mypic{6}
		\caption{Completion of a vector sequence.}
		\label{fig:completion}
	\end{figure}
	\medbreak

	Let $R$ and $I$ be two sets satisfying \ref{it:star}.
	We want to express that \dimens{\vecseqofsets\Asetreset\Asetinc} is unbounded.
	%Thanks to Lemma~\ref{lem:unbounded degree},
	%it suffices to show that for each condition used in the lemma,
	It suffices to show that for each condition in Lemma~\ref{lem:unbounded degree},
	there exists a formula of \msoW which says that the condition
	is satisfied by \vecseqofsets\Asetreset\Asetinc.
	We treat the two cases separately.
	\begin{enumerate}[leftmargin=0em,itemindent=1.5em]
		\item We want to say that there exists a sub-extraction \Avecseqbis of \vecseqofsets\Asetreset\Asetinc
			such that \toinf\Avecseqbis and \dimens\Avecseqbis is unbounded.
			Sub-extraction can be simulated in \mso according to the picture in Figure~\ref{fig:sub-extraction},
			and the condition ``\toinf\Avecseqbis and \dimens\Avecseqbis is unbounded''
			can be checked using Lemma~\ref{lem:msowdef:ttoinf_and_dimttoinf}.
			\begin{figure}[h]
				\centering
				\mypic[.85]{5}
				\caption{Sub-extraction of a vector sequence.}
				\label{fig:sub-extraction}
			\end{figure}
		\item We want to say that
			there exists an interval-closed sub-extraction \Avecseqbis of \vecseqofsets\Asetreset\Asetinc
			such that \Avecseqbis is bounded and \dimens\Avecseqbis is unbounded.
			We use the same approach as in the previous item, \ie,~we simulate sub-extraction
			in the logic using the encoding from Figure~\ref{fig:sub-extraction}, thus obtaining $R',I'$.
			Additionally, we ensure that the selected sub-extraction is interval-closed.
			Importantly, taking an interval-closed sub-extraction preserves property \ref{it:star}.
			It remains therefore to write a formula in \msoW which says that
			sets $R',I'$ satisfy
		%	\ref{it:star},
			\vecseqofsets{R'}{I'} is bounded
			and \dimens{\vecseqofsets{R'}{I'}} is unbounded.
			\begin{itemize}[nosep]
			%	\item Property \ref{it:star} is clearly expressible in \mso.
				\item The boundedness of \vecseqofsets{R'}{I'} is expressed in \msoW
					by the fact that the complement of $I'$ has to satisfy the negation of \weakU.
					Indeed, this exactly says that the intervals of $\Asetinc'$ are bounded.
				\item Now, since \vecseqofsets{R'}{I'} is bounded then \dimens{\vecseqofsets{R'}{I'}} is unbounded
					\iof the sequence of the number of occurrences of elements of $I'$ between two consecutive elements of $R'$ is unbounded. 
					Consider the set $X$ of all the positions which are either in $R'$ or not adjacent to an element in $I'$
					(which is expressible in \mso).
					Then, since $R'$ and $I'$ satisfy $(\star)$,
					\dimens{\vecseqofsets{R'}{I'}} is unbounded \iof \weakU[X] holds.
			\end{itemize}
	\end{enumerate}
	This completes the reduction of Lemma~\ref{lem:msowdef:dimtoinf} to Lemma~\ref{lem:msowdef:ttoinf_and_dimttoinf}.
\end{proof}
