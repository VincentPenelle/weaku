%!TEX root = ../main.tex

This paper is about monadic second-order logic (\mso) on $\omega$-words.
B\"uchi's famous theorem says that
given an \mso sentence describing a set of $\omega$-words over some alphabet,
one can decide if the sentence is true in at least one $\omega$-word
\cite{buchi62}.
B\"uchi's theorem
along with its proof using automata techniques
have been the inspiration for a large number of decidability results for variants of \mso,
including Rabin's theorem on the decidability of \mso on infinite trees \cite{Rabin69}.

One of the themes developed in the wake of B\"uchi's result is the question:
what can be added to \mso on $\omega$-words so that the logic remains decidable?
One direction, studied already in the sixties,
has been to extend the logic with predicates such as
\emph{``position $x$ is a square number''} or \emph{``position $x$ is a prime number''}.
See~\cite{Bes02,KLM15} and the references therein
for a discussion on this line of research.
Another direction,
which is the one taken in this paper,
is to study quantifiers which bind set variables.
These new quantifiers may for instance
talk about the amount of sets satisfying a formula,
hence being midway between the universal and the existential quantifiers,
like the quantifier \emph{``there exists uncountably many sets''}~\cite{BKR11}
(which \aposteriori does not extend the expressivity of \mso
on finitely branching trees).
Another way is to consider quantifiers that talk about the asymptotic behaviors of infinite sets.
This was already the direction followed in \cite{bcc14},
where the authors define \emph{\amso}, a logic which talks about the asymptotic behaviors of sequences of integers
in a topological flavour.
Another example is \emph{the quantifier \quantifierU}
which was introduced in~\cite{Boj04}
and that says that some formula \Aform[X] holds for arbitrarily large finite set~$X$.
%For some time \msoU  was a candidate for an extension of \mso
%with decidable satisfiability,
%with promising results on decidable fragments typically using quantification only over finite sets~\cite{Boj04}.
However, in~\cite{BPT16} it was shown that this logic has undecidable satisfiability;
see~\cite{Boj15} for a discussion on \msoU,
namely \mso extended with the quantifier \quantifierU.
%When extending \mso with a predicate or a quantifier \Aquantifier,
%we use the notation \msoQ.

In this work,
we study a new quantifier for \mso.
Indeed, we consider a quantifier which talks about ultimately periodic sets.
Recall that a set of positions $X\subseteq\Nat$ is called \emph{ultimately periodic}
if there is some period $\Aperiod\in\Nat$ such that for sufficiently large positions $x\in\Nat$,
either both or none of $x$ and $x+\Aperiod$ belong to $X$.
%One can add reasoning about ultimately periodic sets to \mso in two ways,
%which are easily seen to be equivalent:
%(a) the predicate $P$ stating that ``$X$ is an ultimately periodic set'';
%or (b) a set quantifier which ranges only over ultimately periodic sets.
%One can add reasoning about ultimately periodic sets to \mso
%by adding a set quantifier which ranges only over ultimately periodic sets:
We consider the logic \msoP,
\ie, \mso augmented with \emph{the quantifier \quantifierP} that ranges over ultimately periodic sets:
\begin{equation*}
	\quantifierP[X]\Aform[X]:
	\text{``the formula \Aform[X] is true for all ultimately periodic sets $X$''}.
\end{equation*}
\begin{example}
	\label{ex:uperiodic}
	The language of $\omega$-words over $\set{a,b}$
	such that the positions labeled by $a$ form an ultimately periodic set
	is definable in \msoP by the following formula:
	\begin{equation*}
		\exists X
		\quad
		\underbrace{\vphantom{\Big(}\forall x \quad \big(x\in X \iff a(x)\big)}_{\text{$X$ is the set of positions labeled by $a$}}
		\quad
		\wedge
		\qquad
		\underbrace{\neg\Big(\quantifierP Y\quad\big(X\neq Y\big)\Big)}_{\text{$X$ is ultimately periodic}}
		\text.
	\end{equation*}
\end{example}

Though our quantifier \quantifierP extends the expressivity of \mso
(the language from Example~\ref{ex:uperiodic} is not definable in \mso),
it is \apriori not clear whether it has decidable or undecidable satisfiability.
The main result of this paper,
which answers an open question raised in~\cite{BC06},
is that \msoP has undecidable satisfiability.%
\begin{theorem}
	\label{thm:periodic}
	Satisfiability over $\omega$-words is undecidable for \msoP.
	%extended with the quantifier% \quantifierP{}.
%	\begin{equation*}
%		\quantifierP X\Aform[X]:
%		\mbox{``the formula \Aform[X] is true for all ultimately periodic sets $X$''}.
%	\end{equation*}
\end{theorem}
This result is obtained by a reduction from
the above-mentioned logic \msoU,
which has undecidable satisfiability over $\omega$-words \cite{BPT16},
where the quantifier \quantifierU is defined by:
\begin{equation*}
	\quantifierU X\Aform[X]:
	\text{``for all $k \in \mathbb{N}$, \Aform[X] is true for some finite set $X$ of size at least $k$''}
	\text.
\end{equation*}
The logic \msoU was also used in \cite[Theorem~13]{bcc14}
where it is reduced to \amso;
however,
it is not clear
how to compare the expressive power of \msoP
with the one of \amso over $\omega$-words.
%
In our case, the reduction from \msoU to \msoP
is obtained by a sequence of reductions,
involving other variants of \mso
that are obtained by adding some second-order predicates.
We first consider the logic \msoW,
namely \mso extended with \emph{the second-order predicate \weakU} defined by:
\begin{equation*}
	\text{\weakU[X]}:
	\text{``for all $k \in \mathbb{N}$, there exist two consecutive positions of $X$ at distance at least $k$''}
	\text.
\end{equation*}
\begin{example}[from \cite{BC06}]
	Consider the language of $\omega$-words of the form $a^{n_1}ba^{n_2}b\cdots$
	such that $\set{n_1,n_2,\ldots}$ is unbounded.
	This language can be defined in \msoU by a formula
	saying that there are factors of consecutive $a$'s of arbitrarily large size.
	It can also be defined in \msoW
	saying that the set of the positions labeled by $b$ satisfies the predicate \weakU.
\end{example}
It is easy to see that the predicate \weakU can be defined in the logic \msoU:
a set $X$ satisfies \weakU[X]
\iof there exist intervals (finite connected sets of positions) of arbitrarily large size
which are disjoint with $X$.
Therefore, the logic \msoW can be seen as a fragment of \msoU.
Is this fragment proper?
The principal technical contribution of this paper,
which implies Theorem~\ref{thm:periodic},
is showing that actually the two logics are the same:
\begin{theorem}\label{thm:main}
	The logics  \msoU and \msoW define the same languages of $\omega$-words, and translations both ways are effective.
\end{theorem}
\begin{proof}[Proof of Theorem~\ref{thm:periodic} assuming Theorem~\ref{thm:main}]
	Since \msoU has undecidable satisfiability \cite{BPT16},
	the same follows for \msoW by Theorem~\ref{thm:main}.
	Moreover, the predicate \weakU can be defined in terms of ultimate periodicity: 
	a set $X$ of positions satisfies \weakU[X]
	\iof
	for all infinite ultimately periodic sets $Y$,
	there are at least two positions in $Y$
	that are not separated by a position from $X$.
	It follows
	that every sentence of \msoW can be effectively rewritten
	into a sentence of \msoP
	which is true on the same $\omega$-words.
	Hence,
	Theorem~\ref{thm:periodic} follows.
\end{proof}

\subparagraph*{Outline of the paper.}
The rest of the paper is mainly devoted to the proof of Theorem~\ref{thm:main}.
In Section~\ref{sec:level0} we introduce an intermediate logic \msoS.
We first prove that \msoU and \msoS are effectively equivalent
(Section~\ref{ssec:from-qU-to-pU2}, Lemma~\ref{lem:u-predicate}).
Then, we prove that \msoS and \msoW are also effectively equivalent
(Section~\ref{ssec:from-pU2-to-pU1}, Lemma~\ref{lem:u-predicates}),
assuming a certain property, namely Lemma~\ref{lem:msowdef:dimtoinf}.
The proof of this latter lemma is the subject of Section~\ref{sec:unbounded dimensions}.
Finally, in Section~\ref{sec:projection},
we discuss the expressive power of \msoP
with respect to \msoU.
We show that the property ``ultimately periodic'' can be expressed in \msoU
if allowing a certain encoding.
