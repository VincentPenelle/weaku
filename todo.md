+ "proof of Lemma 7 assuming Lemma 8". "we can construct a J⊆I" [[18:10:22 seems done -V]]
+ Section 5: reference to [4, Lemma 3.1] instead of [4, Lemma 2.2]. Done.
+ picture interval-closed extraction: check "," [[18:10:22 still not done -V]]
+ picture sub-extraction: delete empty vector. Done
+ end of section 4 -- Reworked it a bit. V.
+ definition of S-synchronisation. -- Tried to do it. V.
+ check ie. Done.
+ "(checking that the complements satisfy U₁ or not)" : the complement of K (resp. K')! Done.
+ add colon in first item (Section 5). Done.
+ proof of Lemma 10, say before last itemize that $X$ contains only positions labeled by $#$. -- What ? You meant "no positions labelled by #", no ? (in which case it is done). V.


+ After Laure's review on 20170725:
    + introduction: insert . after formula in Example 1
	 + "The logic MSO+U was also used… to be reduced…" berk. [[18:10:22 Has already disappeared - V]]
	 + "A short conclusion is given…" if space available, keep it.

