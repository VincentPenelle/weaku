
------------------------------------------------------------
REVIEW 1
------------------------------------------------------------
PAPER: 68
TITLE: Undecidability of MSO+ultimately periodic
AUTHORS: Mikolaj Bojanczyk, Laure Daviaud, Bruno Guillon, Vincent Penelle and A. V. Sreejith

Overall evaluation: -1 (weak reject)

----------- Overall evaluation -----------

The paper consider the classical monadic second-order logic (MSO) on infinite words. It is well-known that the satisfiability problem for this logic is decidable. The current paper tries to extend the logic by a certain predicate and tries to answer whether satisfiability remains decidable or not.

I am lukewarm about this paper. I fail to see the motivation why the new predicate (a set of positions being ultimately periodic) is interesting. The paper does not provide any evidence that this the case. While, the proof of undecidability is nice and well-explained, I don't think it is sufficient to recommend acceptance, considering the fact that proof itself does not provide any concepts that may be of general interest. The paper gives the impression of being a nice exercise rather than studying a well-motivated problem. This is also reflected in the list of references (5 in total and mainly papers by the authors), and the conclusion which consist in a sequence of lemmas and theorems.

------------------------------------------------------------
REVIEW 2
------------------------------------------------------------
PAPER: 68
TITLE: Undecidability of MSO+ultimately periodic
AUTHORS: Mikolaj Bojanczyk, Laure Daviaud, Bruno Guillon, Vincent Penelle and A. V. Sreejith

Overall evaluation: 2 (accept)

------------------------------------------------------------
Overall evaluation
------------------------------------------------------------

The paper compares the expressive powers of different variants of MSO
on natural numbers with comparison predicates. Since MSO[N,<] is known
to be decidable, natural questions concern the relative expressive
powers of extensions of this theory, and in particular their
decidability. Two ways of extending MSO[N,<] consist in
1. adding new types of (first-order or second-order) quantifiers, with
specific constraints on the objets they bind, and
2. adding new predicates to the theory.

Specifically, the authors are mostly concerned with 3 extensions of MSO :
1. The first one, MSO+U, is an extension of MSO[N,<] with a quantifier
U X \varphi(X) that says: "there exists arbitrarily large, finite sets
X such that \varphi(X) holds"; one of the authors had shown in a
previous article that this theory is undecidable.
2. The second one, MSO + "ultimately perioric", is an extension of
MSO[N,<] with a second-order predicate that says: "the set X is
ultimately periodic".
3. The third one, MSO + "arbitrarily large gaps", is an extension of
MSO[N,<] with a second-order predicate that says: "the set X is
infinite and the set N \setminus X contains arbitrarily large
intervals".

The authors' main result is two-fold:
1. They prove that the first extensions #1 and #3 are as expressive as
each other (and therefore are undecidable), and provide effective
translation procedures from one theory to the other;
2. They prove that the extension #2 is at least as expressive as the
other two (and therefore is undecidable too), and provide an effective
translation procedure from extension #3 to extension #2.

In their conclusion, the authors also claim (and provide a very
sketchy proof) that, up to some rewriting and projection step, the
extensions #1 and #2 are as expressive as each other.

Overall, I enjoyed reading this paper, whose main result is both
interesting and easy to state (i.e., as the title says, that MSO +
"ultimately periodic" is undecidable). I was very favorably impressed
by the clever idea of asymptotic mix, thanks to which, given an oracle
for detecting arbitrarily large *intervals*, we end up detecting
arbitrarily large *sets*. Finally, and despite my remarks below, I
found the paper well-wirtten in general.
In particular, the use of pictures throughout the paper is often
well-thought and very helpful to understand the notions and the
arguments involved, in particular definability in MSO and its
variants. I therefore support the idea of accepting this paper.

------------------------------------------------------------
Major remarks - Main concerns
------------------------------------------------------------

I have three main concerns, which I believe are easy to address (hence
should not be held against this paper), although I think that it is
important that the authors address them before giving a final version
of their paper.

--- Main Concern #1.

I had a hard time diving into the paper and understanding its structure.
I strongly recommend giving a name to the predicate "X is ultimately
periodic". Indeed, when reading the first half of page 2, it took me
some time to realise that this predicate was *not* a predicate U to
which the notation MSO+U would refer. For instance, I would recommend
naming predicates with the letter P (since their nature is quite
different from that of the quantification addition U), e.g. naming
P_1(X) the predicate "X is ultimately periodic", P_2(X) the predicate
"the set X is infinite and the set N \setminus X contains arbitrarily
large intervals" (now called U_1(X)) and P_3(R,I) the predicate now
called U_2(R,I).

In addition, in that section, I would be happy if, whenever a
statement like "MSO + ultimately asymptotic \supseteq MSO + U_1" or
"MSO + U_1 \subseteq MSO + U" is proved, it appears clearly, in the
center of the line (or something like that), so that the reader cannot
wonder what has already been proved.
The same concern applies to page 4, after the proof of Lemma 3, where
I would like to see clearly that " MSO + U_1 \subseteq MSO + U_2".

--- Main Concern #2.

I do not see to what extent Section 2, despite its name, is an outline
of the structure of the proof. I would rather delay the proof of Lemma
3 to a subsequent section (e.g.a Section 2 ½ that might be inserted
between Sections 2 and 3) and focus only on the structure of the
proof:
- In Section 1 it was proved that MSO + ultimately asymptotic
\supseteq MSO + U_1 and that MSO + U_1 \subseteq MSO + U.
- We introduce the predicate U_2, then mention Lemma 3 and say that it
will be proved in Section 2 ½.
- We introduce the vector sequence f_{R,I}, then mention Lemma 5 and
say that il will be proved in Section 4.
- We mention Lemma 4 and say that il will be proved, based on Lemma 5,
in Section 3.
- We complete the proof of Theorem 2.

--- Main Concern #3.

The proof of Lemma 10 is really too sketchy for me.

The intuition behind the sets I, R and R' is not given, and no formal
proof is given either, hence this proof, in this state, is of little
use. Since the page limit is of 15 pages (including references), i.e.
nearly 3 additional pages might be used, I would need some explanation
of the meaning of these sets.

I could convince me that Lemma 10 was correct only after having
invented the following intuition, which I indicate as a
hastily-thought example of what you might write:

1. Of course, the actual value of those positions occupied by #
symbols (i.e. that belong to I) is not important as soon as, in two
consecutive symbols, there is at least one # symbol
(the "tends to infinity" part is exactly the technical reason why you
need Proposition 9, and that forces you to use projection). Below, we
only consider that symbols # have been inserted *between* the symbols
of \Sigma, and we view the sets R and R' as subsets of the positions
in the word w_1 w_2 ... \in \Sigma^{\omega}.

2. We observe that dim(f_{R,I}) is ultimately constant (with limit C)
iff the set R is ultimately of the form \{a + C b | b is a large
enough integer\}.
3. The same observation applies to R', and since R and R' alternate
the constants C and C' must be equal
4. Hence C is a ultimate period of X iff, after some point, either all
or none of the positions in R' belongs to X, and this independently of
the choice of R'.
5. A picture supporting this construction is of course needed.

------------------------------------------------------------
Major remarks - Main suggestions
------------------------------------------------------------

I have also suggestions I'd like to propose to the authors in order to
avoid the heavy machinery used in [end of page 4, page 6 and page 7],
i.e. for shortening Sections 2 ½ and 3.

--- Main Suggestion #1.

Section 2 (in the part I suggest moving to Section 2 ½) consists in
proving that, given some formula \varphi, there exists sets R and I,
characterised in MSO, such that [ U X \varphi(X) ] holds iff
dim(f_{R,I}) is unbounded.
Then, Section 3 consists in proving that, given some sets R and I,
there exists sets R' and I', characterised in MSO, such that
dim(f_{R,I}) is unbounded iff [ dim(f_{R',I'}) is unbounded and
f_{R',I'} -> \infty ]

One can go much faster:

1. Replace the predicate U_2(R,I) by a predicate U'_2(R,I) saying that
" the sequence of vectors encoded by R and I is defined, tends to
infinity, and its dimension is unbounded ".

Here is a translation from MSO + U into MSO + U'_2:

For checking that the predicate U X \varphi(X) holds, we check whether
there exists sets R and I such that the predicate U'_2(R,I) holds and,
for all consecutive elements r_1 and r_2 of R, there exists a set X
such that \varphi(X) holds, and every lower or upper element of every
interval of I \cap [r_1,r_2] belongs to X.

1.a. If U X \varphi X holds, then your proof of Lemma 3 can be adapted
easily: after the n-th element of R (let us say r_n) has been chosen,
pick some set X such that \varphi(X) holds, and with at least n^2
elements x_0 < x_2 < ... < x_{n^2-1} elements greater than r_n. Then,
we add to I the intervals [x_0,x_{n-2}], [x_n,x_{2n-2}],...,
[x_{n(n-1)},x_{n^2-2}], and we choose r_{n+1} = x_{n^2-1}.
1.b. If U X \varphi X does not hold, then every set X \cap [r_1,r_2]
is of cardinality at most K (for some K that depends only on \varphi),
hence the set I \cap [r_1,r_2] contains at most K intervals, and the
sequence f_{R,I} is of bounded dimension, if it is ever well-defined.

Of course, the translation from MSO + U'_2 to MSO + U is not as
immediate as it was with your choice of MSO + U_2, but
- we can still do it, since checking that a sequence tends to infinity
(i.e. that all its subsequences are unbounded) and checking that its
dimension is unbounded are feasible in MSO+U;
- we could also wait for translating MSO + U'_2 to MSO + U_1, which
was already translated to MSO + U.

2. Since checking that some sequence of vectors f_{R,I} is
well-defined and tends to infinity is feasible in MSO + U_1, it
suffices to apply Lemma 7.

------------------------------------------------------------
Main Suggestion #1b.
------------------------------------------------------------

Alternatively, we may just go faster for Section 3 alone, i.e. say
that checking that dim(f_{R,I}) is unbounded is feasible as soon as
checking that [ f_{R,I} -> \infty and dim(f_{R,I}) is unbounded ] is
feasible, although the construction is less nice in this case:

We replace both sets R and I by sets R' and I' such that
1.a R' is an infinite subset of R
1.b. for every two consecutive elements r_1 and r_2 or R, if they both
belong to R', then I' \cap [r_1,r_2] is formed of intervals whose
endpoints are endpoints of intervals of I \cap [r_1,r_2]
1.c. if r_1 does not belong to R', then [r_1, r_2-1] \subseteq I', and
if r_2 does not belong to R', then [r_1+1,r_2] \subseteq I'.

Then, if dim(f_{R,I}) is bounded, dim(f_{R,I}) is smaller, hence it is
bounded as well. However, if dim(r_{R,I}) is unbounded, we proceed as
follows, starting from a counter c and an element r of R:

2.a Choose some element r_1 of R such that r_1 > r + c and such that,
if r_2 is the next element of R, the set I \cap [r_1,r_2] contains at
least c^2 intervals [x_1,y_1],...,[x_{c^2},y_{c^2}];
2.b Add r_1 and r_2 to R', and add the intervals [r+1,r_1-1],
[x_1,y_c], [x_{c+1}, y_{2c}], ..., [x_{c^2-c+1}, y_{c^2}] to I'.
2.c Repeat the process, starting from the counter value c+1 and from
the element r_2.

Obviously f_{R',I'} tends to infinity and its dimension is unbounded.

------------------------------------------------------------
Minor remarks
------------------------------------------------------------

- Page 2: there is a typo (line 6), a plural form of the word " set "
should be used.

- Page 3: Is the analogy with counters, drawn when the proof of Lemma 3
begins, ever used afterwards? This analogy did not help me, and I felt
slightly confused because I was afraid of missing something.

- Page 4: The argument about the set J follows directly a description of
vector sequences. We should be warned, between these two paragraphs,
that we go from a descriptive part to a proof part (intended to reduce
the cases under scrutiny to a special case)

- Page 7: It is mentioned twice that the first condition of Lemma 6 is
expressible in MSO+U_1 (thanks to Lemma 5): once in lines 2 and 3
(with a very short sketch of proof), once in lines 4-6 after Figure 3
(with a more substantial proof). This confused me when reading it for
the second time, since I was not sure anymore of what had already been
proved or not. The distinction between "sketch of proof / outline" and
"actual proof" should be made clearer.

- Page 9: The property P should rather be named P(h,g), since it refers
explicitly to the sequences h and g. In particular, saying that "given
a vector sequence f with tends towards infinity, if it admits a strict
sub-extraction g satisfying (P)" is meaningless, since P refers to h
and g, not to g only (or it would implicitly refer to h and g, not to
f and g).

- Page 9: Lemma 2.2 from the reference [4] is incorrectly restated (and
the current statement is false): property (1) is "for infinitely many
positions i, the i-th vector of f has a higher dimension than the i-th
vector of g". In addition:
	- condition (2) is just (\neg P(f,g));
	- in stating this lemma, I recommend speaking of vector sequences 
h and g rather than f and g, since it is h that we will use in practice
(see my remark above);
	- in general, there is a confusion between f and h.

- I would rather restate the proof of the " if " direction as follows:
If f has bounded dimension, let h be a sub-sequence of f, and let g be
a strict extraction of h. By construction, both g and h tend to
infinity and have bounded dimension. Then, [4, Lemma 2.2] states that,
since for infinitely many positions i the i-th vector of h has a
higher dimension than the i-th vector of g, the property \neg P(h,g)
holds.

------------------------------------------------------------
REVIEW 3
------------------------------------------------------------
PAPER: 68
TITLE: Undecidability of MSO+ultimately periodic
AUTHORS: Mikolaj Bojanczyk, Laure Daviaud, Bruno Guillon, Vincent Penelle and A. V. Sreejith

Overall evaluation: 1 (weak accept)

------------------------------------------------------------
Overall evaluation 
------------------------------------------------------------
The paper aims at showing that extending MSO with a second-order predicate
asserting that a set of positions is ultimately periodic results in an
undecidable logic, called MSO+"ultimately periodic".
The proof is based on eventually showing that MSO+"ultimately periodic" capture
the logic MSO+U, shown to be undecidable in a previous work.
The proof is organized in several steps, during which two second-order
predicates U_1 and U_2 (and the corresponding extension of MSO respectively called 
MSO+U_1 and MSO+U_2) are considered and proved to be expressively complete for 
MSO+U; undecidability of MSO+"ultimately periodic" then follows from the observation
that predicate U_1 can be expressed in terms of the "ultimate periodicity" predicate.
As a further result, the authors provide a converse translation, that is,
MSO+"ultimately periodic" can be encoded in MSO+U modulo a certain encoding of
the models (omega-words)

The result is not trivial and pretty nice, with some of the techniques involved
being nice and ingenious. However, I have some concerns (see below) preventing 
me  from giving  a fully positive evaluation to the paper.

------------------------------------------------------------
Main concerns
------------------------------------------------------------

- I am not sure if the result, while being non-trivial, is strong enough to give
raise to a publication to CSL. Maybe, such a technical and specific result should
be proposed to a less broad conference.

- No argument is given by the authors for supporting the significance and the
relevance of their undecidability result, even though there is room for a discussion 
(the paper spans slightly more than 11 pages, while the conference imposes a 
15-page limit).

- The authors assume the reader to be familiar not only with the logic MSO, but also
with its extension MSO+U, which is surely not as standard as the former.
Only an informal definition of the semantics of the U quantifier is given, and, in
my opinion, it is a bit ambiguous for those who are not already familiar with
MSO+U, as it is not clear if the set X is existentially or universally quantified.
I think that the paper would be accessible to a larger audience if a little effort
would be made to clarify at least the semantics of the U quantifier.
As an example, I found the following formulations less ambiguous:

   UX \varphi(X) iff \varphi(X) is satisfied by arbitrarily large finite sets

   UX \varphi(X) iff there are finite sets X of arbitrary large size for which
                     \varphi(X) is true

- I see a problem with the argument used at page 7. At lines 4/5 it is said that
you "need that the gaps between consecutive intervals of I have bounded length"
and then you show how to obtain a set J from (R,I) such that gaps between
consecutive intervals of J (and belonging to the same "R-section") have bounded
length and, in addition, dim(f_{R,I}) is unbounded iff dim(f_{R,J}) is unbounded.
I do not see why the last claim holds. In particular, the "if" direction of the
claim is not clear at all to me. It might be that I am missing something obvious
here and that, as you say, the claim clearly holds, but some additional explanation 
would definitely help.

- There might be a potential problem with the proof of Lemma 4:
-- property (1) in [4, Lemma 2.2] is not the same as (1) here. They are similar
but it is not immediate to see that they are equivalent-- I do not see why f and
g satisfy both (1) and (2)


------------------------------------------------------------
Specific comments
------------------------------------------------------------

- p. 1, l. -11 (line 11 from the bottom): "discussion of MSO+U" -> "discussion
on MSO+U"

- p. 2, l. 8: "for all infinite ultimately periodic set" -> "for all infinite
ultimately periodic sets"

- p. 2, l. 14: "if and only there exists" -> "if and only if there exist"

- p. 2, l. -1: "lemma is essentially" -> "lemma follows essentially"

- p. 3: in the statement of property (\star). Add a \newline before the last
sentence "Here is a picture of property (\star)".

- second to last line of proof of Lemma 3: "such that ... and ... holds for some
n" -> "such that ... and ... hold for some n"

- first line after the  proof of Lemma 3: remove the sentence "The construction in
Lemma 3 is easily seen to be effective" because what follows is not a consequence 
of the effectiveness of the construction but rather a consequence of the effectiveness 
of the translation.

- remove a comma at the end of the interval-closed extraction and a comma at the
end of the sub-extraction.

- p. 6, l. -12: "Let us mark the coordinates smaller than m" -> "Let us mark the
coordinates smaller than or equal to m"

- p. 6, l. -11: "with at least n consecutive marked coordinates" -> "with more
than n consecutive marked coordinates"

- p. 6, l. -9: "greater than n(m+2)+m" -> "greater than n(m+1)+m"

- p. 6, l. -9/-8: "i.e., m+2 blocks" -> "i.e., m+1 blocks"

- p. 6, l. -6: "for each vector the maximal block" -> "for each vector a longest
maximal block"

- p. 7, l. 1: "Consider R and I two sets" -> "Let R and I be two sets"

- p. 7, l. 5: In my opinion, the sentence: "This is ensured by extending the intervals"
is not clear and confusing. Just remove it or replace it with something more generic 
like: "We proceed as follows".

- p. 7, l. 2 after Figure 3: "Thanks to Lemma 6, it suffices to show that for
each condition used in the lemma" -> "It suffices to show that for each
condition in Lemma 6"

- p. 7, l. 3 of item 2: "using the encoding from Figure 4" -> "using the
encoding from Figure 4, thus obtaining R',I'"

- p. 8, l. 8/9 of Section 4: give a definition of restriction of f to S (in the current
version, you simply introduce the notation for it); even just a definition via an 
example would help. The way it is, it was not clear to me, thus making unclear 
the definitions of asymptotically equivalent and asymptotic mix as well.

- p. 8, l. -4/-3: "if at least one coordinate by vector of f has been popped in
g" -> "if at least one coordinate for each vector of f has been removed in g"

- remove the \newline at line 3 of the proof of Lemma 7

- p. 9 (middle): should "so have f" be rephrased as "so does f" or "so has f"?
(I am now sure about this)

- p. 9 (middle): I would not use the notation "dim(h) -> \infty".
Even if it might be pretty intuitive, it is a bit confusing with the notation "h
-> \infty" used throughout the paper.
I would rather say "dim(h) tends towards infinity, that is, for all n there
exists i such that for every j > i the dimension of the j-th vector of h is
bigger than n"

- p. 9, example relative to the definition of {\bf g'}: I think you should
remove the first column (the one with all "(1)"), because every vector in h has
dimension at least 2 and that column is confusing.
Removing the first column is also coherent with the example at the bottom of the
page relative to the definition of {\it g}.

- p. 9, l. -2 (before the example at the bottom): "every vector in the sequence
{\bf g} exists" -> "every vector in the sequence {\bf g'} exists"

- p. 10, l. 4: "is greater than n" -> "is at least n"

- p. 10, l. 4/5: "tend to infinity" -> "tend towards infinity"

- p. 10, l. 6: "all its coordinates" -> "all of its coordinates"

- p. 10, l. 7: "the n-th coordinate of {\bf g'}" -> "the n-th coordinate of the
i-th vector of {\bf g'}"

- p. 10 (middle): the sentence "We say that an encoding of a vector sequence S,J
is S-synchronized" should be rephrased into a clearer, probably expanded form.

- p. 11, l. 1 of Theorem 8: "MSO with the predicate" -> "MSO augmented with the
predicate".
Same applies to the first line of Lemma 10

- p. 11, Theorem 8: define transformation \pi (at least say that it is the
obvious extension to languages of transformation \pi_\Sigma over words)

- p. 11, Proposition 9: recall the meaning of dim(f_{R,I}) being ultimately
constant

- p. 11: I found it really difficult for me (impossible) for me to understand the 
proof of Lemma 10. I think that a more detailed proof is needed, taking into
account that the authors have almost three additional pages at their disposal.

- p. 11, first item in the proof of Lemma 10: "X and R contains" -> "X and R
contain"

- p. 11, last item in the proof of Lemma 10: "for every infinite set R' ... is
ultimately constant, then all elements" -> "for every infinite set R' ... is
ultimately constant, we have that all elements"

- item 4 in Section "References" is incomplete.