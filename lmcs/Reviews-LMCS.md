#Reviewer 1:

Undecidability of a Weak Version of MSO+U

This is a nice paper that studies MSO+U_1, a simplified version of the logic
MSO+U. The main result is that, over omega-words, this logic has the same
expressive power as MSO+U. In particular, the satisfiability problem is
undecidable. As an application the authors also show that MSO+P, the extension
of MSO with quantifiers over ultimately periodic sets, is undecidable as well
(by a reduction to MSO+U_1).

These results are interesting and deserve to be published.
In addition the paper is extremely well-written and a real pleasure to read.
I have only found a few minor places where the paper could be improved.

Let me start with a general remark. I do not much like the habit of the authors
to give proofs in a piecemeal fashion and to defer parts of a proof to a later
section. If overdone, this writing style obscures the structure of the proof.
In my opinion it is okay to do this kind of thing once in a proof, but not
several times in a row. If I had written the paper, I would probably have added
a section collecting all results pertaining to vector sequence and then did the
remaining coding arguments in a following section. Of course this is a matter
of taste, so I don't require the authors to change their exposition. But I hope
that, for their next paper, they might at least consider whether my critique
has some merits.

page 2, line 11: "the amount of sets" sounds strange to me. What about "the
number of sets"? -- Done -V.

page 2, line -13: "being AT MOST 19" -- done -V.

page 4, line 8: "The language also admitS" -- done -V.

page 6: I do not like the terminology of "vector sequences". These are not
vectors, but tuples. (Or which vector space are we talking about?)
-- That is, I think, a problem of naming culture. As far as I know, it is classical
in our community to mix vectors and tuples (e.g. in Vector Addition System), so I vote for not
taking that into account. Maybe explain that in the answer. -V.

/!\ proof of Lemma 2.4: I think the proof can be simplified. You do not need to ensure
property (*). Instead, in the last step you can define the set X as containing
all elements that do not belong to an interval [x,y] with x,y in I and such
that [x,y] is disjoint from R.
-- Hum, I think he's not entirely wrong, but a bit incorrect. I think, the correct version of what he means
is that, instead of completing the sequences, and then checking the number of elements not in I' between two 
consecutive elements in R is finite, we could actually directly define R as the neighbourhood of I, and check that 
the intersection of said neighbourhood with intervals between two consecutive elements of R is finite.
That seems true, but I don't think that is really simpler, so I don't think we should change it. Plus, I vaguely
remember we already discussed that, and there was a very good reason we did what we did, but I don't remember
it now :-(. -V.
-- He is wrong. You may have $f_{R,I}$ bounded, $dim(f_{R,I})$ bounded as well, but have unbounded gaps between x and y (the min and max I-element between two successive elements from R). That is, the following equivalence does not hold with his encoding:
«Now, since $f_{R',I'}$ is bounded
then $dim(f_{R',I'})$ is unbounded
if and only if
the sequence of the number of occurrences of elements of I'
between two consecutive elements of R'
is unbounded.» -B.
-- Ah, you're right Bruno, I was probably a bit rust when I read his comment :-). -V.

page 10, line 13: "1-extractionS" -- corrected «for all 1-extraction» to «for every 1-extraction», I thing it is more correct. -V.

line 19: "is said TO BE strict" -- done. -V

line 19: "one coordinate OF EACH vector" -- done. -V.

line 21: "admitS" -- done. -V.

/?\ line 23: "of AN asymptotic mix" -- I disagree, I think «the notion of asymptotic mix» is correct. -V. -- Ok. -B.

 line -9: "and g be A STRICT extraction of h" -- Done, I think he's right (when I compile, it's line -6, by the way. -V.

/!\ page 11: In the proof of Lemma 3.1 you do not mention that you also need to express that
f_R,I -> infty. Please add a short explanation of how to do so.
-- Done, but I may have been clumsy (and I'm a bit afraid this is not the right place for that. - V.
-- I reformulated a bit Vincent's explanation (splitting the sentence in two). -B.
-- Great! That's better. -V.

/!\ proof of Lemma 4.4: Please give a few more details for the base case of the induction.
For instance, which vocabulary are you using? I assume you use the ordering but not
the successor relation. (This makes a difference.)
-- There is indeed a slight problem in what we wrote : the identity does not work, as we need to ignore all # when we count positions (like in successor, the translation is actually, the next non-# position. So I've tried to write it, but feel free to modify it, or tell it's not sufficient. What the reviewer suggests may be correct, but more painful to write, as then we probably should put elsewhere (like the intro) what is exactly the signature we consider. What do you think? - V.
-- If think what you did is good and sufficient (I just changed 'letter' to 'position'). -B.

#Reviewer 2:

The paper considers an extension of monadic second-order logic (MSO)
over infinite words by a predicate U1 that holds for a set of positions
if one can find arbitrary large gaps between successive positions in
the set.

Logics and automata that can express such forms of unboundedness
conditions have been studied intensively for finite and infinite words
and trees in the last years, resulting in a rich theory with
interesting applications to decision problems for classical automata
and logics.

The main result of the paper is the undecidability (of satisfiability)
of the proposed logic. For the proof, the authors show that the new
logic is effectively equivalent to the logic MSO+U which is known to
be undecidable. It follows that also the logic MSO+P with a quantifier
over ultimately periodic sets is undecidable because the predicate U1
can easily be expressed with such a quantifier. This answers an open
question from another paper.
For the other direction, it remains unclear whether MSO+P formulas can
be rewritten into equivalent MSO+U formulas. But the authors show that
it can be done up to some encoding of the defined languages.

The paper is rather easy to read (for someone familiar with MSO on
infinite words). The proof consists of a sequence of reductions, and
all of them are explained very clearly with nice graphical
illustrations.

I consider the results in the paper an interesting and non-trivial
contribution. I therefore recommend to accept the paper (maybe after
some very minor revision, see comments below).


Details:

/!\ - p 1,l -2: "the result in X...". There is a missing citation. -- I don't know which one. -V.
-- Probably [10] -B.
-- Indeed, Mikołaj said that as well. Done. -V.

/?\ - p 8, proof of Lemma 2.4: After reading the thrid line of the proof,
  I thought that the first condition is done, and that the rest of the
  proof is devoted only to the second condition. This confused me,
  because the transformation leading to (*) does not preserve the
  second condition. Then I realized that the preparation for (*) is
  done in general, and after that both cases are treated. Maybe there
  is a way to avoid this confusion. (But this is a minor point.)
-- Well, I'm not sure to understand the problem here. That seems clear to me. Anyway, it seems minor even to the
reviewer, so I vote not to change anything. -V.
-- I understand what he said, but I am not sure we can do better. Ok. -B.

- p 10, 4 lines above Lemma 3.3: "dimenstion less than 2 admit" -> "...admits" -- done. - V. 

/?\ - p 12, end of Section 3: It seems that you use K,K' for the
  1-extratctions and also for the sub-sequences that are compared for
  asymptotical equivalence. This makes the last lines of the proof a
  bit difficult to read.
-- I don't see his problem, that's the only place we use K and K' in the whole section, and we indeed
use them for the 1-extraction. So, that seems ok to me. - V.
-- I see it. When we say «(checking that the complement of K (resp. K') satisfy U₁ or not)»,
we don't really mean K and K', but a subset of it, as we kept only
«maximal intervals in K and K' that directly follow the selected elements» of S,
when taking a subsequence.
I changed accordingly, but, in order to avoid introducing too many set variable names,
I just removed the detailed parenthesis.
I think this is clear enough. -B.
-- Ok. -V.
