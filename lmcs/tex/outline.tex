In this section,
we introduce a new predicate on pairs of sets of positions \strongU[R,I]
and consider the logic \mso extended with this predicate: \msoS.
We first prove in Section~\ref{ssec:from-qU-to-pU2},
that \msoU has the same expressive power as \msoS.
Then, we show in Section~\ref{ssec:from-pU2-to-pU1},
%that in order to prove Theorem~\ref{thm:main},
%it suffices to prover
that \msoS is itself as expressive as \msoW,
assuming a certain property, namely Lemma~\ref{lem:msowdef:dimtoinf}.
The proof of this lemma is given in Section~\ref{sec:unbounded dimensions}.

\subsection{From quantifier \ensuremath{\texttt U} to predicate \texorpdfstring{\ensuremath{\texttt U_2}}{U2}}
\label{ssec:from-qU-to-pU2}
We say that a sequence of natural numbers is \emph{unbounded}
if arbitrarily large numbers occur in it.
For two disjoint sets of positions $R, I \subseteq \Nat$
with $R$ infinite, we define a sequence of numbers as in Figure~\ref{fig:first-encoding}:
the $i$-th element of this sequence is the number of elements from $I$ between the $i$-th
and the $(i+1)$-th elements from $R$.
In particular, elements of $I$ in positions smaller than all the positions from $R$ are not relevant.
\begin{figure}[h]
	\mypic{3}
	\caption{%
		Two sets of positions $R,I \subseteq \Nat$ and the sequence in $\Nat^\omega$ that they define.
		The sequence is only defined when $R$ and $I$ are disjoint, and $R$ is infinite.%
	}
	\label{fig:first-encoding}
\end{figure}

We now define the binary predicate \strongU:
\begin{equation*}
	\strongU[R,I]:
	\text{
		``the sequence of numbers encoded by $\Asetreset,\Asetinc$
		%(according to Figure~\ref{fig:first-encoding})
		%(Figure~\ref{fig:first-encoding})
		is defined and unbounded''%
	}.
\end{equation*}
The difference between the predicate \weakU,
which has one free set variable,
and the predicate \strongU,
which has two free set variables,
is that the latter is able to ignore some positions. It is illustrated
by the following example,
which can be easily defined in \msoS.
%but for which a definition in \msoW is not straightforward.
\begin{example}
	Let $L$ be the union of the languages of $\omega$-words
	of the form $(ac^*)^{n_1}b(ac^*)^{n_2}b\cdots$
	where $\set{n_1,n_2,\ldots}$ is unbounded.
	It can be defined in \msoS in the following way:
	an $\omega$-word belongs to $L$ \iof
	it belongs to $\big({(ac^*)}^*b\big)^\omega$
	%it has infinitely many $b$'s
	and the sequence of numbers encoded by $R=\set{\text{set of positions labeled by $b$}}$
	and $I=\set{\text{set of positions labeled by $a$}}$ is unbounded.
	The language also admits a simple definition in \msoU:
	there exist arbitrarily large finite sets of positions labeled by $a$
	such that no two positions from the set are separated by a $b$.
	It is however not straightforward to define it in the logic \msoW,
	since in every $\omega$-word,
	the distance between two successive $b$'s separated by at least one $a$
	can be increased by adding occurrences of $c$'s,
	without changing the membership of the word to the language.
	%It is however not clear
	%how to define it
	%in \msoW since it is not sufficient to ask
	%that the set of positions labeled by $b$ satisfies \weakU because of the arbitrary number of $c$'s between two $b$'s.
\end{example}

The following lemma follows essentially from the same argument as in~\cite[Lemma 5.5]{BC06}.
\begin{lemma}\label{lem:u-predicate}
	The logics \msoU and \msoS
	define the same languages of $\omega$-words,
	and translations both ways are effective.
\end{lemma}
\begin{proof}
	The predicate \strongU is easily seen to be expressible in \msoU:
	a sequence of numbers encoded by $R$ and $I$ is unbounded if the
	subsets of elements of $I$ between two consecutive elements of $R$
	are of arbitrarily large size, which is expressible in \msoU.
	For the converse implication, we use the following observation.
	A formula $\quantifierU[X]\Aform[X]$ is true if and only if the following condition holds:
	\begin{description}
		\item[{\namedlabel[$\mathbf{(\star)}$]{it:starstar}{$(\star)$}}]
			There exist two sets $\Asetreset,\Asetinc\subseteq\Nat$
			which satisfy \strongU[R,I]
			such that for every two consecutive positions $\Avarreset,\Avarresetbis\in\Asetreset$,
			there exists some finite \Asetvar satisfying \Aform[X]
			which contains all positions of \Asetinc between \Avarreset and \Avarresetbis.
			Here is a picture of property \ref{it:starstar}:

			\noindent\mypic{1}
	\end{description}
	Condition \ref{it:starstar} is clearly expressible in \mso with the predicate \strongU.
	Hence the lemma will follow once we prove the equivalence:
	$\quantifierU[X]\Aform(X)$ \iof \ref{it:starstar}.
	The right-to-left implication is easy to see.
	For the converse implication, we do the following construction.
	We define a sequence of positions
	$0=x_0<x_1<\ldots$
	as follows by induction.
	Define $x_0$ to be the first position,
	\ie,~the number $0$.
	Suppose that $x_n$ has already been defined.
	By the assumption $\quantifierU[X]\Aform[X]$, there exists a set $X_{n+1}$ which satisfies $\Aform$
	and which contains at least $n$ positions after $x_{n}$.
	Define $x_{n+1}$ to be the last position of $X_{n+1}$.
	This process is illustrated in the following picture:

	\noindent\mypic{4}
	Define $R$ to be all the positions $x_0,x_1,\ldots$ in the sequence thus obtained,
	and define $I$ to be the set of positions $x$
	such that $x_n<x<x_{n+1}$ and $x \in X_{n+1}$ holds for some $n$.
	By construction, the sets $I$ and $R$ thus obtained will satisfy \strongU.
\end{proof}

\subsection{From predicate \texorpdfstring{\ensuremath{\texttt U_2}}{U2} to predicate \texorpdfstring{\ensuremath{\texttt U_1}}{U1}}
\label{ssec:from-pU2-to-pU1}
Lemma~\ref{lem:u-predicate} above
states that \msoU and \msoS have the same expressive power,
thus giving a first step towards the proof of Theorem~\ref{thm:main}.
The second step, which is the key point of our result,
is the following lemma,
which states that \msoS is as expressive as \msoW.
\begin{lemma}
	\label{lem:u-predicates}
	The logics \msoS and \msoW
	define the same languages of $\omega$-words,
	and translations both ways are effective.
\end{lemma}
Note that one direction is straightforward,
since \weakU[X] holds \iof \strongU[X,\mathbb N\setminus X] holds.
Hence, it remains to show that the predicate \strongU
can be defined by a formula of the logic \msoW with two free set variables.
\smallbreak

In our proof, we use terminology and techniques about sequences of vectors of natural numbers
that were used in the undecidability proof for \msoU~\cite{BPT16}.
A \emph{vector sequence} is defined to be an element of $\left(\Nat^*\right)^\omega$,
\ie, a sequence of possibly empty tuples of natural numbers.
For a vector sequence $\Avecseq\in\left(\Nat^*\right)^\omega$,
we define its \emph{dimension}, denoted by $\dimens\Avecseq\in\Nat^\omega$,
as being the number sequence of the dimensions of the vectors:
the $i$-th element in \dimens\Avecseq is \emph{the dimension of the $i$-th vector} in \Avecseq,
\ie,~the number of coordinates in the $i$-th tuple in \Avecseq.
We say that a vector sequence \Avecseq \emph{tends towards infinity}, denoted by \toinf\Avecseq,
if every natural number appears in finitely many vectors from \Avecseq.

Recall Figure~\ref{fig:first-encoding},
which showed how to encode a number sequence using two sets of positions $\Asetreset,\Asetinc\subseteq\Nat$.
We now show that the same two sets of positions can be used to define a vector sequence.
As it was the case in Figure~\ref{fig:first-encoding},
we assume that \Asetreset is infinite and disjoint from \Asetinc.
Under these assumptions, we write $\vecseqofsets\Asetreset\Asetinc\in\left(\Nat^*\right)^\omega$
for the vector sequence defined according to the description from Figure~\ref{fig:encoding},
\ie, the sequence of vectors whose coordinates are the lengths of intervals in \Asetinc
between two consecutive elements of \Asetreset.
Remark that $0$ is not encoded
and thus that $\vecseqofsets\Asetreset\Asetinc$ contains possibly empty vectors
with only positive coordinates.
As for Figure~\ref{fig:first-encoding},
the positions of $I$ smaller than all the positions of $R$ are not relevant.
\begin{figure}[h]
	\mypic{2}%
	\caption{%
		Two sets of positions $\Asetinc,\Asetreset\subseteq\Nat$
		and the vector sequence that they encode.
		This vector sequence is defined only when \Asetinc and \Asetreset are disjoint and \Asetreset is infinite.%
	}%
	\label{fig:encoding}%
\end{figure}
\smallbreak

The following lemma states
that the unboundedness of the dimensions of a vector sequence can be expressed in \msoW.
Its proof will be the subject of Section~\ref{sec:unbounded dimensions}.
\begin{lemma}
	\label{lem:msowdef:dimtoinf}
	There is a formula \Aform[R,I] in \msoW
	which is true \iof
	the vector sequence \vecseqofsets\Asetreset\Asetinc is defined
	and satisfies \dimens{\vecseqofsets\Asetreset\Asetinc} is unbounded.
\end{lemma}
\begin{proof}[Proof of Lemma~\ref{lem:u-predicates} assuming Lemma~\ref{lem:msowdef:dimtoinf}]
	As the predicate \weakU is easily definable in \msoS
	(\weakU[X] holds \iof \strongU[X,X\setminus\Nat] holds),
	it remains to prove the converse,
	\ie, to exhibit a formula \Aform[X,Y] of \msoW
	which holds \iof \strongU[X,Y] holds.

	Since \strongU[X,Y] holds if and only if \strongU[X,Y\setminus X] holds
	and because \strongU[X,Y] implies that $X$ is infinite,
	it is sufficient to write a formula \Aform[\Asetreset,\Asetinc] which holds
	\iof \strongU[\Asetreset,\Asetinc] holds,
	assuming \Asetreset and \Asetinc are disjoint
	and \Asetreset is infinite.
	We fix such \Asetreset and \Asetinc,
	which hence define a vector sequence \vecseqofsets\Asetreset\Asetinc
	according to the encoding of Figure~\ref{fig:encoding}.
	Furthermore,
	\strongU[R,I] holds
	\iof summing all coordinates of each vector from the sequence
	yields a number sequence which is unbounded.
	We show that we can construct a $\Asetincbis\subseteq\Asetinc$
	such that \strongU[R,I] holds
	\iof the dimension of \vecseqofsets\Asetreset\Asetincbis is unbounded.%

	For every non negative integer $x$,
	if $2x$ belongs to \Asetinc
	as well as
	either $2x+1$ or $2x-1$,
	then we remove $2x$ from \Asetinc.
	Let \Asetincbis denote the thus obtained set.
	As at least one position from \Asetinc over two is kept in \Asetincbis,
	we have that \strongU[\Asetreset,\Asetinc] holds
	\iof
	\strongU[\Asetreset,\Asetincbis] holds.
	Furthermore, \Asetincbis
	has the property that each of its point is isolated
	whence the sum of the coordinates of a vector from \vecseqofsets\Asetreset\Asetincbis
	is equal to its dimension.
	Therefore,
	\strongU[\Asetreset,\Asetincbis] holds \iof
	the dimension of the vector sequence \vecseqofsets\Asetreset\Asetincbis is unbounded.
	Moreover, \Asetincbis is definable in \mso
	in the sense that there is a \mso-formula with two free set variables \Asetinc and \Asetincbis
	which holds if and only if \Asetincbis is obtained from \Asetinc by the process given above.

	We conclude the proof of Lemma~\ref{lem:u-predicates}
	by applying Lemma~\ref{lem:msowdef:dimtoinf}.
%
%	Therefore, thanks to Lemma~\ref{lem:u-predicate}, Lemma \ref{lem:msowdef:dimtoinf} implies Theorem \ref{thm:main}.
\end{proof}

