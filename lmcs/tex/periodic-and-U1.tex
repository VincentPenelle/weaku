We consider now the quantifier \quantifierP defined in the introduction.
Recall that a set of positions $X\subseteq\Nat$ is called \emph{ultimately periodic}
if there is some period $\Aperiod\in\Nat$ such that for sufficiently large positions $x\in\Nat$,
either both or none of $x$ and $x+\Aperiod$ belong to $X$.
%One can add reasoning about ultimately periodic sets to \mso in two ways,
%which are easily seen to be equivalent:
%(a) the predicate $P$ stating that ``$X$ is an ultimately periodic set'';
%or (b) a set quantifier which ranges only over ultimately periodic sets.
%One can add reasoning about ultimately periodic sets to \mso
%by adding a set quantifier which ranges only over ultimately periodic sets:
We consider the logic \msoP,
\ie, \mso augmented with \emph{the quantifier \quantifierP} that ranges over ultimately periodic sets:
\begin{equation*}
	\quantifierP[X]\Aform[X]:
	\text{``the formula \Aform[X] is true for all ultimately periodic sets $X$''}.
\end{equation*}
\begin{example}
	\label{ex:uperiodic}
	The language of $\omega$-words over $\set{a,b}$
	such that the positions labeled by $a$ form an ultimately periodic set
	is definable in \msoP by the following formula:
	\begin{equation*}
		\exists X
		\quad
		\underbrace{\vphantom{\Big(}\forall x \quad \big(x\in X \iff a(x)\big)}_{\text{$X$ is the set of positions labeled by $a$}}
		\quad
		\wedge
		\qquad
		\underbrace{\neg\Big(\quantifierP Y\quad\big(X\neq Y\big)\Big)}_{\text{$X$ is ultimately periodic}}
		\text.
	\end{equation*}
\end{example}

From Example~\ref{ex:uperiodic}, one can see that our quantifier \quantifierP extends strictly the expressivity of \mso, but it is \apriori not clear whether it has decidable or undecidable satisfiability. However, using Theorem~\ref{thm:main}, we can prove Theorem~\ref{thm:periodic} which states that satisfiability over $\omega$-words is undecidable for \msoP.

\begin{proof}[Proof of Theorem~\ref{thm:periodic}]
	Since \msoU has undecidable satisfiability \cite{BPT16},
	the same follows for \msoW by Theorem~\ref{thm:main}.
	Moreover, the predicate \weakU can be defined in terms of ultimate periodicity: 
	a set $X$ of positions satisfies \weakU[X]
	\iof
	for all infinite ultimately periodic sets $Y$,
	there are at least two positions in $Y$
	that are not separated by a position from $X$.
	It follows
	that every sentence of \msoW can be effectively rewritten
	into a sentence of \msoP
	which is true on the same $\omega$-words.
	Hence,
	Theorem~\ref{thm:periodic} follows.
\end{proof}

The predicate \weakU
can thus be expressed in \mso augmented with the quantifier \quantifierP.
It is not clear that the converse is true, and we leave this as an open problem.
However, in Theorem~\ref{thm:projections} below,
we show that,
up to a certain encoding, all languages expressible in \msoP can be expressed in \msoU.

Let $\Sigma$ be a finite alphabet,
\freshletter be a symbol not belonging to $\Sigma$,
and $\Sigma_\freshletter$ denote $\Sigma\cup\set\freshletter$.
We define
$\pi_\Sigma: {\Sigma_\freshletter}^\omega\to\Sigma^\omega$
to be the function which erases all appearances of \freshletter.{}
This is a partial function,
because it is only defined on $\omega$-words over $\Sigma_\freshletter$
that contain infinitely many letters from $\Sigma$.
We extend $\pi_\Sigma$ to languages of $\omega$-words in a natural way.
\begin{theorem}\label{thm:projections}
	Every language definable in \msoP over $\Sigma$
	is equal to $\pi_\Sigma(L)$
	for some language $L\subseteq{\Sigma_\freshletter}^\omega$ definable in \msoU.
\end{theorem}

The main difficulty in expressing ``ultimate periodicity''
is to check the existence of a constant, namely the period,
which is ultimately repeated.
Such a property was already the crucial point in the proof of the undecidability of \msoU \cite{BPT16}.
Indeed the authors managed to express
that an encoded vector sequence (as in Figure~\ref{fig:encoding})
tending towards infinity
has \emph{``ultimately constant dimension''},
\ie, all but finitely many vectors of the sequence have the same dimension
(this dimension will represent the period). This is the content of the following lemma which is a direct consequence of \cite[Lemma~3.1]{BPT16}.
\begin{lemma}
	\label{lem:ultimately constant}
	There exists a formula \Aform[R,I] in \msoU
	with two free set variables
	which holds \iof
	\vecseqofsets RI is defined,
	tends towards infinity,
	and \dimens{\vecseqofsets RI} is ultimately constant.
\end{lemma}

In order to use Lemma \ref{lem:ultimately constant}, we will encode a word 	
$w$ over $\Sigma$ by adding factors of consecutive \freshletter
between every two letters from $\Sigma$,
such that the lengths of those factors tend towards infinity.
%In order to prove Theorem~\ref{thm:projections},
%we will padd the $\omega$-word using \freshletter,
%so that the vector sequences we consider will tend towards infinity.
%
Let $w=w_1w_2\cdots$ be an $\omega$-word over $\Sigma$
where the $w_i$'s are letters,
and let $\Anumseq=n_1,n_2,\ldots$ be a number sequence.
We define the $\omega$-word
$w_\Anumseq$ over $\Sigma_\freshletter$
as $w_1\freshletter^{n_1}w_2\freshletter^{n_2}\cdots$.
In particular $\pi_\Sigma(w_\Anumseq)=w$.
Now, given a sentence \Aform of \msoP over $\Sigma$,
we consider the language $L$ over $\Sigma_\freshletter$
of all the $\omega$-words of the form $w_\Anumseq$
for $w$ satisfying \Aform
and \Anumseq a number sequence tending towards infinity.
As
we can easily check in \msoU
that the sequence of lengths of the factors of \freshletter
tends towards infinity,
%there is a formula in \msoU satisfied exactly
%by the $\omega$-words of the form $w_\Anumseq$
%such that \Anumseq tends towards infinity,
Theorem~\ref{thm:projections} follows immediately from the following lemma.
%for the language $L$ defined above.
\begin{lemma}
	For every sentence \Aform in \msoP,
	one can effectively construct a formula $\Aform_\freshletter$ in \msoU
	such that 
	for every $\omega$-word $w\in\Sigma^\omega$,
	the following conditions are equivalent:
	\begin{enumerate}[nosep]
		\item \Aform is true in $w$;
		\item $\Aform_\freshletter$ is true in $w_f$ for every $f$ which tends towards infinity.
	\end{enumerate}
\end{lemma}
\begin{proof}
	Let \Aform be a sentence in \msoP, and $f$ some number sequence tending
	towards infinity.
	Given a word $w$, we construct $w_f$ as explained above.
	
	We construct $\Aform_\freshletter$ by induction on the structure of 
	\Aform, and show that at every step of the induction, \Aform is true in $w$
	if and only of $\Aform_\freshletter$ is true in $w_f$.
	To do so, we use the natural mapping from positions of $w$ into positions of $w_f$
	to handle free variables.
	In particular, every free set variable used in the induction hypothesis
	is supposed to contain only positions not labeled by \freshletter.
	%
	%	If \Aform is a conjunction (resp. disjunction or negation), $\Aform_\freshletter$ is the conjunction (resp. disjunction, negation)
	%	of the result of the induction over its subformul\ae{}.
	%	The quantifications over sets (\resp positions) are translated to quantifications over sets containing no $\freshletter$ (\resp positions not labeled $\freshletter$).
	%	The atoms other than ultimately periodic are translated to themselves.
	%	%while ensuring their argument is not a \freshletter (or does not contain \freshletter).
	Every formula not containing the quantifier $\quantifierP$ is translated
	straightforwardly, only ignoring all \freshletter positions when counting
	(a bounded number, of course).
	For example, the successor relation will be translated by
	''the first position to the right not labelled with \freshletter''.
	It is routine to check that the satisfiability is the same
	in both $w$ and $w_f$, thanks to the natural mapping described above.
	The key point of the induction
	is the case of a formula $\quantifierP[X]\Aform[X]$.
	We translate it to a formula of the form: $\forall Y, 
	\mathrm{UP}_\freshletter(Y) \Rightarrow
	\Aform_\freshletter(Y)$, where $\mathrm{UP}_\freshletter(Y)$ holds 
	for all sets $Y$ which contain only positions not labeled by $\freshletter$
	and which are ``ultimately periodic when ignoring \freshletter's''.
	%	(representing the images of the sets $X$ \bg{what does it mean?}\bg{I understood, but it is not clear when reading sequentially.}),
	%the translation obtained for \Aform by induction must be true for $Y$.

	Checking if $Y$ does not contain any position labeled by $\freshletter$
	is done by the formula $\forall y, y\in Y \Rightarrow \neg \freshletter(y)$.
	It remains to express the property ``$Y$ is ultimately periodic when ignoring \freshletter's''
	as a formula of \msoU with one free set variable $Y$ which contains only positions not labeled by \freshletter.
	It can be done in the following way:
	``$Y$ is ultimately periodic when ignoring \freshletter's'' \iof
	there exist two sets $R$ and $I$ such that:
	\begin{itemize}
			%	\item $X$ contains no \freshletter;
		\item $I$ is exactly the set of all the $\freshletter$,
			$R$ is infinite and disjoint from $I$
			%	(\ie, contain no \freshletter)
			(hence \vecseqofsets RI is defined); 
		\item \dimens{\vecseqofsets RI} is ultimately constant
			(this constant dimension will represent the period of $Y$); 
		\item for every infinite set $R'$ disjoint from $I$,
			alternating with $R$
			(\ie, such that there is exactly one element of $R'$ between two consecutive elements of $R$)
			and such that \dimens{\vecseqofsets{R'}{I}} is ultimately constant, we have that
			all elements of $R'$ are either ultimately in $Y$ or ultimately not in $Y$.
%			\vp{Trying to give a bit of intuition. May be useless.}
%			Informally, we thus capture all $R'$ ultimately of the form $\{x,x+p,x+2p,\cdots\}$ with $p$ the same period
%			as $Y$. By definition of ultimate periodicity, these sets are ultimately either included in $Y$ or disjoint from it.
	\end{itemize}
	These properties can be expressed in \msoU
	thanks to Lemma~\ref{lem:ultimately constant},
	since, as $f$ tends towards infinity, the vector sequence \vecseqofsets RI,
	tends towards infinity. It is easy to check that the conjunction
	of these three properties is equivalent to 
	``$Y$ is ultimately periodic when ignoring \freshletter's''.
	
	
	%Moreover, the satisfiability of the formula $\Aform_\freshletter$ over $w_f$ only depends on the fact that $\vecseqofsets{R}{I}$ tends towards infinity and on the satisfiability of $\Aform$ over $w$. Therefore, every $w_f$ with $f$ satisfying the latter satisfies $\Aform_\freshletter$ if and only if $w$ satisfies $\Aform$.
\end{proof}

