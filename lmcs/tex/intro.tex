%!TEX root = ../main.tex

This paper is about monadic second-order logic (\mso) on $\omega$-words.
B\"uchi's famous theorem says that
given an \mso sentence describing a set of $\omega$-words over some alphabet,
one can decide if the sentence is true in at least one $\omega$-word
\cite{buchi62}.
B\"uchi's theorem
along with its proof using automata techniques
have been the inspiration for a large number of decidability results for variants of \mso,
including Rabin's theorem on the decidability of \mso on infinite trees \cite{Rabin69}.

By now it is quite well understood that \mso is a maximal decidable logic over finite words. One formalisation of this can be found in \cite{LZ} (Theorem 9), which shows that the class of regular languages is the maximal class which: (a) has decidable satisfiability; and (b) is closed under Boolean operations and images under rational relations. The general idea behind the result in \cite{LZ} is that a non-regular language has a Myhill-Nerode equivalence relation of infinite index, and this together with closure under Boolean operations and images under rational relations can be used to generate counters, Turing machines, and then arbitrary languages in the arithmetic hierarchy. However, for languages of infinite words, the situation is different as logic over omega-words can talk about asymptotic properties, and getting a counter from asymptotic properties can be much harder.
One of the themes developed in the wake of B\"uchi's result is the question:
what can be added to \mso on $\omega$-words so that the logic remains decidable?
One direction, studied already in the sixties,
has been to extend the logic with predicates such as
\emph{``position $x$ is a square number''} or \emph{``position $x$ is a prime number''}.
See~\cite{Bes02,KLM15} and the references therein
for a discussion on this line of research.
Another direction,
which is the one taken in this paper,
is to study quantifiers which bind set variables.
These new quantifiers may for instance
talk about the number of sets satisfying a formula,
hence being midway between the universal and the existential quantifiers,
like the quantifier \emph{``there exists uncountably many sets''}~\cite{BKR11}
(which \aposteriori does not extend the expressivity of \mso
on finitely branching trees).
Another way is to consider quantifiers that talk about the asymptotic behaviors of infinite sets.
This was already the direction followed in \cite{bcc14},
where the authors define \emph{\amso}, a logic which talks about the asymptotic behaviors of sequences of integers
in a topological flavour.
Another example is \emph{the quantifier \quantifierU}
which was introduced in~\cite{Boj04}
and that says that some formula \Aform[X] holds for arbitrarily large finite set~$X$:
\begin{equation*}
	\quantifierU X\Aform[X]:
	\text{``for all $k \in \mathbb{N}$, \Aform[X] is true for some finite set $X$ of size at least $k$''}
	\text.
\end{equation*}
%For some time \msoU  was a candidate for an extension of \mso
%with decidable satisfiability,
%with promising results on decidable fragments typically using quantification only over finite sets~\cite{Boj04}.
However, in~\cite{BPT16} it was shown that
\msoU,
namely \mso extended with the quantifier \quantifierU,
has undecidable satisfiability;
see~\cite{Boj15} for a discussion on this logic.
%When extending \mso with a predicate or a quantifier \Aquantifier,
%we use the notation \msoQ.

In this work, we study a, \apriori, weaker version of \msoU, the logic \msoW,
namely \mso extended with \emph{the second-order predicate \weakU} defined by:
\begin{equation*}
	\text{\weakU[X]}:
	\text{``for all $k \in \mathbb{N}$, there exist two consecutive positions of $X$ at distance at least $k$''}
	\text.
\end{equation*}

\begin{example}
\ 

\begin{itemize}
\item The set $X = \{10 n\mid n\in \mathbb{N}\}$ does not satisfy \weakU, as for every $n$, $10(n+1) - 10n = 10$, so there are no two consecutive positions at distance at least $11$.
\item The set $X = \{2^n \mid n\in \mathbb{N}\}$ does satisfy \weakU, as for every $k$, $2^{k+1} - 2^{k} > k$, and $2^{k+1}$ and $2^k$ are consecutive in $X$.
\item The set $X = \{10m + n \mid \text{n is the }m^{\text{th}}\text{ digit of }\pi\}$ does not satisfy \weakU, the difference between two consecutive elements being at most $19$, as there is always an element between $10m$ and $10m+9$.
\end{itemize}
\end{example}

\begin{example}[from \cite{BC06}]
	Consider the language of $\omega$-words of the form $a^{n_1}ba^{n_2}b\cdots$
	such that $\set{n_1,n_2,\ldots}$ is unbounded.
	This language can be defined in \msoU by a formula
	saying that there are factors of consecutive $a$'s of arbitrarily large size.
	It can also be defined in \msoW
	saying that the set of the positions labeled by $b$ satisfies the predicate \weakU.
\end{example}

It is easy to see that the predicate \weakU can be defined in the logic \msoU:
a set $X$ satisfies \weakU[X]
\iof there exist intervals (finite connected sets of positions) of arbitrarily large size
which are disjoint with $X$.
Therefore, the logic \msoW can be seen as a fragment of \msoU.
Is this fragment proper?
The main contribution of this paper
is showing that actually the two logics are the same:
\begin{theorem}\label{thm:main}
	The logics  \msoU and \msoW define the same languages of $\omega$-words, and translations both ways are effective.
\end{theorem}

We believe
that \msoW can be reduced to many extensions of \mso, in a simpler way
than reducing \msoU.

As an example, we consider a quantifier which talks about ultimately periodic sets.
A set of positions $X\subseteq\Nat$ is called \emph{ultimately periodic}
if there is some period $\Aperiod\in\Nat$ such that for sufficiently large positions $x\in\Nat$,
either both or none of $x$ and $x+\Aperiod$ belong to $X$.
For example, the set $\{10n \mid n\in\mathbb{N}\}$ is periodic (with period 10), but the set $\{10m + n \mid \text{n is the }m^{\text{th}}\text{ digit of }\pi\}$ is not periodic.
We consider the logic \msoP,
\ie, \mso augmented with \emph{the quantifier \quantifierP} that ranges over ultimately periodic sets:
\begin{equation*}
	\quantifierP[X]\Aform[X]:
	\text{``the formula \Aform[X] is true for all ultimately periodic sets $X$''}.
\end{equation*}
Though quantifier \quantifierP extends the expressivity of \mso,
it is \apriori not clear whether it has decidable or undecidable satisfiability.
However, using Theorem~\ref{thm:main}, we obtain the following result, which answers an open question raised in~\cite{BC06}.
\begin{theorem}
	\label{thm:periodic}
	Satisfiability over $\omega$-words is undecidable for \msoP.
	%extended with the quantifier% \quantifierP{}.
%	\begin{equation*}
%		\quantifierP X\Aform[X]:
%		\mbox{``the formula \Aform[X] is true for all ultimately periodic sets $X$''}.
%	\end{equation*}
\end{theorem}

Another example is the logic \mso+probability as defined in \cite{MM}. This line of thought is not developed in the present article, but we can make the following reasoning.  A set X satisfies $\weakU(X)$ if and only if there exists an infinite set $Y$ of blocks in $X$ with the following property: there is nonzero probability of choosing a subset $Z \subseteq \mathbb{N}$ such that in every block from $Y$, there is at least one element of $Z$.  Therefore, \mso+probability is more expressive than $\msoW$.



%The logic \msoU was also used in \cite[Theorem~13]{bcc14}
%where it is reduced to \amso;
%however,
%it is not clear
%how to compare the expressive power of \msoP
%with the one of \amso over $\omega$-words.
%%
%In our case, the reduction from \msoU to \msoP
%is obtained by a sequence of reductions,
%involving other variants of \mso
%that are obtained by adding some second-order predicates.
%We first consider the logic \msoW,
%namely \mso extended with 


\subsection*{Outline of the paper.}
The rest of the paper is mainly devoted to the proof of Theorem~\ref{thm:main}.
In Section~\ref{sec:level0} we introduce an intermediate logic \msoS.
We first prove that \msoU and \msoS are effectively equivalent
(Section~\ref{ssec:from-qU-to-pU2}, Lemma~\ref{lem:u-predicate}).
Then, we prove that \msoS and \msoW are also effectively equivalent
(Section~\ref{ssec:from-pU2-to-pU1}, Lemma~\ref{lem:u-predicates}),
assuming a certain property, namely Lemma~\ref{lem:msowdef:dimtoinf}.
The proof of this latter lemma is the subject of Section~\ref{sec:unbounded dimensions}.
Finally, in Section~\ref{sec:projection},
we discuss the expressive power of \msoP
with respect to \msoU. We prove Theorem~\ref{thm:periodic} and we show that the property ``ultimately periodic'' can be expressed in \msoU
if allowing a certain encoding.
