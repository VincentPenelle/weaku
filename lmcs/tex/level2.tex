\subsection{Unboundedness of dimensions for sequences tending to infinity}
%\subsection{Proof of Lemma~\ref{lem:msowdef:ttoinf_and_dimttoinf}}
\label{ssec:level2}
We now prove Lemma~\ref{lem:msowdef:ttoinf_and_dimttoinf}
whence concluding the proof of Theorem~\ref{thm:main}.
The lemma says that
there is a formula \Aform[\Asetreset, \Asetinc] in \msoW
which is true \iof
the vector sequence \vecseqofsets\Asetreset\Asetinc is defined and satisfies
\toinf{\vecseqofsets\Asetreset\Asetinc} and
\dimens{\vecseqofsets\Asetreset\Asetinc} is unbounded.

%Observe that only the condition \toinf{\dimens{\vecseqofsets\Asetreset\Asetinc}}
%is not clearly definable in \msoW.
%The lemma says that in the case of vector sequences tending toward infinity,
%we manage to express the property.
%\medbreak

The main idea of the proof is to relate the unboundedness of the dimension
with a property definable in \msoW, namely the \emph{asymptotic mix} property,
which was already used in \cite{BPT16} to prove undecidability of \msoU.
This is done in Lemma~\ref{lem:asymptotic-mix} below,
for which we need a couple of notions on vector sequences.
\medbreak

Given an infinite set $\Asetindex \subseteq \Nat$ of positions and a number sequence $\Anumseq \in \Nat^\omega$,
the \emph{restriction of \Anumseq to \Asetindex} is the number sequence obtained by
discarding elements of index not in $\Asetindex$ and is denoted \restrict\Anumseq\Asetindex.
Two number sequences \Anumseq and \Anumseqbis are called \emph{asymptotically equivalent},
denoted by $\Anumseq\asymequiv\Anumseqbis$,
if they are bounded on the same sets of positions,
\ie, if for every set \Asetindex,
\restrict\Anumseq\Asetindex is bounded \iof \restrict\Anumseqbis\Asetindex is bounded.
For example, the number sequences
$\Anumseq=1,1,3,1,5,1,7,1,9,\ldots$
and $\Anumseqbis=2,1,4,1,6,1,8,1,10,\dots$
are asymptotically equivalent,
but $\Anumseq$ and $\Anumseqter=1,3,1,5,1,7,1,9,1,\ldots$ are not.

We say that a vector sequence $\Avecseq$ is an \emph{asymptotic mix} of a vector sequence $\Avecseqbis$
if for every $1$-extraction $\Anumseq$ of $\Avecseq$,
there exists a $1$-extraction $\Anumseqbis$ of $\Avecseqbis$
such that $\Anumseq\asymequiv\Anumseqbis$.
In particular, the empty vector cannot occur in \Avecseq or \Avecseqbis.
Note that this is not a symmetric relation.

Given two vector sequences \Avecseq and \Avecseqbis,
we say that \Avecseqbis \emph{dominates} \Avecseq, denoted $\Avecseq\leq\Avecseqbis$,
if for all \ANindex,
the \ANindex-th vectors in both sequences have the same dimension,
and the \ANindex-th vector of \Avecseq is coordinatewise smaller than or equal
to the \ANindex-th vector of \Avecseqbis.
Furthermore, we suppose that all the coordinates in \Avecseq and \Avecseqbis are positive. 

An extraction \Avecseqbis of \Avecseq is said to be \emph{strict},
if at least one coordinate of each vector of \Avecseq has been popped in \Avecseqbis,
\ie, the dimensions of the vectors of \Avecseqbis are pointwise smaller than those of \Avecseq.
In particular, a vector sequence with some vectors of dimension less than $2$ admits no strict extraction,
nevertheless, it may admit strict sub-extractions. 

The following lemma relates the notion of asymptotic mix with the unboundedness of the dimension of a vector sequence,
providing the vector sequence tends towards infinity.
\begin{lemma}
	\label{lem:asymptotic-mix}
	Let \Avecseq be a vector sequence such that \toinf\Avecseq.
	Then, \dimens\Avecseq is unbounded
	\iof
	there exists a sub-sequence \Avecseqter
	and a strict extraction \Avecseqbis of \Avecseqter satisfying:
	\begin{description}
		\item[{\namedlabel%
					[{\textcolor{darkgray}{\textbf{($P_{\Avecseqbis,\Avecseqter}$)}}}]%
					{eq:dominate->asympt-mix}%
					{($P_{\Avecseqbis,\Avecseqter}$)}%
				}%
			]
			for every $\Avecseqter'\leq\Avecseqter$,
			there exists $\Avecseqbis'\leq\Avecseqbis$
			such that
			$\Avecseqter'$ is an asymptotic mix of $\Avecseqbis'$.
	\end{description}
\end{lemma}
\begin{proof}
	The if direction follows from \cite[Lemma~2.2]{BPT16}, while the converse is new to this work.
	More precisely, it is proved in \cite[Lemma~2.2]{BPT16}
	that given two vector sequences \Avecseqter and \Avecseqbis tending towards infinity,
	if \Avecseqter and \Avecseqbis have bounded dimension,
	then the two following properties are equivalent:
	\begin{description}[topsep=1ex,labelsep=.2em]
		\item[{\namedlabel[{\textcolor{darkgray}{\textbf{(1)}}}]{p1}{(1)}}]
			for infinitely many $i$'s,
			the $i$-th vector of \Avecseqter has higher dimension than the $i$-th vector of \Avecseqbis;
		\item[{\namedlabel[{\textcolor{darkgray}{\textbf{(2)}}}]{p2}{(2)}}]
			the negation of \ref{eq:dominate->asympt-mix}:
			there exists $\Avecseqter'\leq\Avecseqter$
			which is not an asymptotic mix of any $\Avecseqbis'\leq\Avecseqbis$.
	\end{description}
	Let \Avecseq be a vector sequence which tends towards infinity
	and suppose that it has bounded dimension.
	Let \Avecseqter be a sub-sequence of \Avecseq
	and \Avecseqbis be a strict extraction of \Avecseqter.
	By definition, \Avecseqter and \Avecseqbis satisfy \ref{p1},
	have bounded dimension
	and tend towards infinity.
	Therefore, by \cite[Lemma~2.2]{BPT16},
	they satisfy \ref{p2},
	\ie,
	the negation of \ref{eq:dominate->asympt-mix}.
	\medbreak

	We now prove the only if direction.
	We suppose that \Avecseq tends towards infinity and that \dimens\Avecseq is unbounded.
	There exists a sub-sequence \Avecseqter of \Avecseq such that
	every vector occurring in \Avecseqter has dimension at least $2$
	and \dimens\Avecseqter tends towards infinity.

	Define \Avecseqbis as being the extraction obtained from \Avecseqter
	by dropping the last coordinate in each vector
	and observe that it is a strict extraction of \Avecseqter.
	We prove now a result stronger than required,
	by exhibiting a universal $\Avecseqbis'\leq\Avecseqbis$
	which makes \ref{eq:dominate->asympt-mix}
	true whatever the choice of $\Avecseqter'\leq\Avecseqter$,
	hence inverting the order of the universal and the existential quantifiers.
	Let $\Avecseqbis'$ be the vector sequence defined as follows:
	for each position \ANindex,
	denoting
	the \ANindex-th vector of $\Avecseqter$
	by $\vect{\Aveccoord_1,\Aveccoord_2,\ldots,\Aveccoord_k}$,
	the \ANindex-th vector of $\Avecseqbis'$ is set to
	$%\[
	\vect{%
		\min\left(\Aveccoord_1,1\right),
		\min\left(\Aveccoord_2,2\right),
		\ldots,
		\min\left(\Aveccoord_{k-1},k-1\right)
	}
	$%\]
	.
	For instance:
	\[
		\begin{array}{rlllllll}
			\Avecseqter=&
			(2,1),&(3,2,1),&(4,3,2,1),&(5,4,3,2,1),&(6,5,4,3,2,1),&\ldots\\
			\Avecseqbis=&
			(2),&(3,2),&(4,3,2),&(5,4,3,2),&(6,5,4,3,2),&\ldots\\
			\Avecseqbis'=&
			(1),&(1,2),&(1,2,2),&(1,2,3,2),&(1,2,3,3,2),&\ldots
		\end{array}
	\]

	We now prove that every $\Avecseqter'\leq\Avecseqter$ is an asymptotic mix of $\Avecseqbis'$,
	that is, for every $1$-extraction $\Anumseqter$ of $\Avecseqter'$, there exists a $1$-extraction $\Anumseqbis$ of $\Avecseqbis'$
	such that $\Anumseqter\asymequiv\Anumseqbis$.
	We fix an arbitrary vector sequence $\Avecseqter'\leq\Avecseqter$
	and a $1$-extraction $\Anumseqter$ of $\Avecseqter'$.
	We define \Anumseqbis as the number sequence
	whose \ANindex-th number,
	for each position \ANindex,
	is the maximal coordinate in the \ANindex-th vector of $\Avecseqbis'$
	which is less than or equal to the \ANindex-th number in \Anumseqter.
	This coordinate always exists since the first coordinate of every vector in the sequence \Avecseqbis' exists and is equal to $1$.
	For instance, according to the previous example and given $\Avecseqter'$ and \Anumseqter,
	the number sequence \Anumseqbis is defined as follows:
	\[
		\begin{array}{rcccccc}
			\Avecseqter'=&
			(1,1),&(2,1,1),&(3,2,1,1),&(4,3,2,1,1),&(5,4,3,2,1,1),&\ldots\\
			\Anumseqter=&
			1,&2,&1,&4,&1,&\ldots\\
			\Avecseqbis'=&
			(1),&(1,2),&(1,2,2),&(1,2,3,2),&(1,2,3,3,2),&\ldots\\
			\hline
			\Anumseqbis=&
			1,&2,&1,&3,&1,&\ldots
		\end{array}
	\]
	Observe that, by definition, for every position \ANindex,
	the \ANindex-th number of \Anumseqbis is less than or equal to
	the \ANindex-th number of \Anumseqter.
	Hence, for every set \Asetindex of positions,
	if \restrict\Anumseqter\Asetindex is bounded then so is \restrict\Anumseqbis\Asetindex.
	Suppose now that for some set \Asetindex of positions,
	\restrict\Anumseqter\Asetindex is unbounded and fix an integer \ANinteger.
	We want to prove that some number in \restrict\Anumseqbis\Asetindex is greater than \ANinteger.
	Since both \Avecseqter and \dimens\Avecseqter tend towards infinity,
	we can find a threshold $\Athreshold\in\Nat$
	such that for every position $\ANindex\geq\Athreshold$,
	the \ANindex-th vector of \Avecseqter has dimension greater than \ANinteger
	and furthermore all its coordinates are greater than \ANinteger.
	Thus, for all positions $\ANindex\geq\Athreshold$,
	the \ANinteger-th coordinate of the \ANindex-th vector of $\Avecseqbis'$ is defined
	and it is equal to \ANinteger.
	Therefore,
	for all $\ANindex\in\Asetindex$ with $\ANindex\geq\Athreshold$
	and such that the \ANindex-th number in \Anumseqter is at least \ANinteger,
	the \ANindex-th number in \Anumseqbis is at least \ANinteger as well.
	Hence, \restrict\Anumseqbis\Asetindex is unbounded.
\end{proof}

To conclude the proof of Lemma~\ref{lem:msowdef:ttoinf_and_dimttoinf},
it remains to express in \msoW the property stated in Lemma~\ref{lem:asymptotic-mix}:
given two sets \Asetreset and \Asetinc
encoding a vector sequence \vecseqofsets\Asetreset\Asetinc
which tends towards infinity,
there exists a sub-sequence \Avecseqter of \vecseqofsets\Asetreset\Asetinc
and a strict extraction \Avecseqbis of \Avecseqter satisfying \ref{eq:dominate->asympt-mix}.
%
Note that we can check in \msoW
that a sequence~$\vecseqofsets\Asetreset\Asetinc$
(defined by two set variables~$\Asetreset$ and~$\Asetinc$)
tends towards infinity.
Indeed, this holds
\iof,
for any bounded set~$Y$ (\ie, such that~$\neg\weakU(Y)$),
ultimately,
the intervals of~$\Asetinc$
contain at least two elements of~$Y$.

Given a vector sequence \Avecseq and a set $S$,
we say that an encoding of \Avecseq is \emph{$S$-synchronised}
if it is of the form $S,J$ for some set $J$.
%We say that an encoding of a vector sequence $S,J$ is \emph{$S$-synchronised}.
Sub-sequences and strict extractions can be simulated in \mso
by defining subsets of the original encoding,
according to the picture in Figure~\ref{fig:sub-extraction}.
In particular,
given \Asetreset and \Asetinc,
one can construct in \mso
any sub-sequence \Avecseqter of \vecseqofsets\Asetreset\Asetinc
and any strict extraction \Avecseqbis of \Avecseqter,
such that the encodings of \Avecseqter and \Avecseqbis are $S$-synchronised for some $S\subseteq R$.

Now, we express \ref{eq:dominate->asympt-mix} on \Avecseqter and \Avecseqbis encoded by $S,J$ and $S,J'$ respectively.
Every dominated sequence $\Avecseqter'$ (\resp $\Avecseqbis'$) of \Avecseqter (\resp \Avecseqbis)
can be obtained by deleting some rightmost consecutive positions of intervals of $J$ (\resp $J'$),
keeping at least one position for each interval.
This can be done in \mso, preserving the $S$-synchronisation, as depicted below:
\smallbreak

\noindent\mypic{11}

Finally, we prove that the property saying that $\Avecseqter'$ is an asymptotic mix of $\Avecseqbis'$
can be expressed in \msoW
(using the $S$-synchronisation of the encodings of the two sequences).
Indeed, $1$-extractions can be simulated in \mso
according to the picture in Figure~\ref{fig:sub-extraction},
still preserving the $S$-synchronisation. 
Consider then $K,K'$ such that $S,K$ encodes a $1$-extraction of $\Avecseqter'$ and $S,K'$ encodes a
$1$-extraction of $\Avecseqbis'$,
\ie, such that between every two consecutive positions of $S$,
the elements of $K$ (\resp $K'$) form an interval.
It remains to prove that 
we can express in \msoW that 
\vecseqofsets SK and \vecseqofsets S{K'},
viewed as number sequences, are asymptotically equivalent.
We select a subset of indices of the two number sequences \vecseqofsets SK and \vecseqofsets S{K'}
by selecting elements of $S$
and keeping the maximal intervals in $K$ and $K'$ that directly follow the selected elements.
Then, we check thanks to \weakU
that the corresponding sequences of numbers
are both bounded or both unbounded.
This process is depicted here:
\smallbreak

\noindent\mypic{12}

